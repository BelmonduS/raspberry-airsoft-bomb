#!/usr/bin/python
# Example using an RGB character LCD connected to an MCP23017 GPIO extender.
import time

import Adafruit_CharLCD as LCD
import Adafruit_GPIO.MCP230xx as MCP
#from Adafruit_MCP230XX import Adafruit_MCP230XX


# Define MCP pins connected to the LCD.
lcd_rs        = 2
lcd_en        = 3
lcd_d4        = 4
lcd_d5        = 5
lcd_d6        = 6
lcd_d7        = 7
lcd_red       = 6
lcd_green     = 7
lcd_blue      = 8

# Define LCD column and row size for 16x2 LCD.
lcd_columns = 16
lcd_rows    = 2

# Alternatively specify a 20x4 LCD.
# lcd_columns = 20
# lcd_rows    = 4

# Initialize MCP23017 device using its default 0x20 I2C address.
gpio = MCP.MCP23017()

# Alternatively you can initialize the MCP device on another I2C address or bus.
# gpio = MCP.MCP23017(0x24, busnum=1)

# Initialize the LCD using the pins
lcd = LCD.Adafruit_RGBCharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7,
                              lcd_columns, lcd_rows, lcd_red, lcd_green, lcd_blue,
                              gpio=gpio)

# Print a two line message
lcd.message('Defusable bomb\r ready')

# Wait 5 seconds
#time.sleep(5.0)

# Demo showing the cursor.
#lcd.clear()
#lcd.show_cursor(True)
#lcd.message('Show cursor')

#time.sleep(5.0)

# Demo showing the blinking cursor.
#lcd.clear()
#lcd.blink(True)
#lcd.message('Blink cursor')

#time.sleep(5.0)

# Stop blinking and showing cursor.
#lcd.show_cursor(False)
#lcd.blink(False)

# Demo scrolling message right/left.
#lcd.clear()
#message = 'Scroll'
#lcd.message(message)
#for i in range(lcd_columns-len(message)):
#    time.sleep(0.5)
#    lcd.move_right()
#for i in range(lcd_columns-len(message)):
#    time.sleep(0.5)
#    lcd.move_left()

# Demo turning backlight off and on.
#lcd.clear()
#lcd.message('Flash backlight\nin 5 seconds...')
#time.sleep(5.0)
# Turn backlight off.
#lcd.set_backlight(0)
#time.sleep(2.0)
# Change message.
#lcd.clear()
#lcd.message('Goodbye!')
# Turn backlight on.
#lcd.set_backlight(1)


import wiringpi as wiringpi
from time import sleep

pin_base = 65  # lowest available starting number is 65
i2c_addr = 0x20  # A0, A1, A2 pins all wired to GND

wiringpi.wiringPiSetup()  # initialise wiringpi
wiringpi.mcp23017Setup(pin_base, i2c_addr)  # set up the pins and i2c address

#wiringpi.pinMode(65, 1)  # sets GPA0 to output
#wiringpi.digitalWrite(65, 0)  # sets GPA0 to 0 (0V, off)

wiringpi.pinMode(80, 0)  # sets GPB7 to input
wiringpi.pullUpDnControl(80, 2)  # set internal pull-up
wiringpi.pinMode(79, 0)  # sets GPB7 to input
wiringpi.pullUpDnControl(79, 2)  # set internal pull-up

# Note: MCP23017 has no internal pull-down, so I used pull-up and inverted
# the button reading logic with a "not"


def get_rotary_state():
    wire_A = 3
    wire_B = 4
    while wire_A != wire_B:
        if not wiringpi.digitalRead(79):  # inverted the logic as using pull-up
            wire_A = 0
        else:
            wire_A = 1
        if not wiringpi.digitalRead(80):  # inverted the logic as using pull-up
            wire_B = 0
        else:
            wire_B = 1

    print("rotary state {0}".format(wire_A))
    return wire_A


def rotation_sequence(self):
    a_state = wiringpi.digitalRead(79)
    b_state = wiringpi.digitalRead(80)
    r_seq = (a_state ^ b_state) | b_state << 1
    return r_seq


A = 1
B = 1
ORIGINAL_STATE = get_rotary_state()
DIRECTION = 10
LOCK = 0

MAIN_MENU = ["Set timer", "Set wires", "Arm"]
CURRENT_MENU = ""

import datetime

from Adafruit_LED_Backpack import SevenSegment

now = datetime.datetime.now()
hour = now.hour
minute = now.minute
second = now.second

segment = SevenSegment.SevenSegment(address=0x70)

# Initialize the display. Must be called once before using the display.
segment.begin()

segment.clear()
# Set hours
segment.set_digit(0, int(hour / 10))  # Tens
segment.set_digit(1, hour % 10)  # Ones
# Set minutes
segment.set_digit(2, int(minute / 10))  # Tens
segment.set_digit(3, minute % 10)  # Ones
# Toggle colon
segment.set_colon(second % 2)  # Toggle colon at 1Hz

# Write the display buffer to the hardware.  This must be called to
# update the actual display LEDs.
segment.write_display()

# Wait a quarter second (less than 1 second to prevent colon blinking getting$
time.sleep(0.25)

try:
    while True:
        A = wiringpi.digitalRead(79)
        B = wiringpi.digitalRead(80)

        if A != B and LOCK == 0:
            if ORIGINAL_STATE == 0:
                if A == 1 and B == 0:
                    DIRECTION = DIRECTION + 0.03
                else:
                    DIRECTION = DIRECTION - 0.03
            else:
                if A == 1 and B == 0:
                    DIRECTION = DIRECTION - 0.03
                else:
                    DIRECTION = DIRECTION + 0.03
            LOCK = 1
        else:
            ORIGINAL_STATE = A
            LOCK = 0

        if DIRECTION > len(MAIN_MENU):
            DIRECTION = 0
        if DIRECTION < 0:
            DIRECTION = len(MAIN_MENU)
        print(DIRECTION)
        if MAIN_MENU[int(DIRECTION)-1] != CURRENT_MENU:
            lcd.clear()
            lcd.message(MAIN_MENU[int(DIRECTION)-1])
            CURRENT_MENU = MAIN_MENU[int(DIRECTION)-1]

finally:
    wiringpi.digitalWrite(65, 0)  # sets port GPA1 to 0 (0V, off)
    wiringpi.pinMode(65, 0)  # sets GPIO GPA1 back to input Mode
    # GPB7 is already an input, so no need to change anything